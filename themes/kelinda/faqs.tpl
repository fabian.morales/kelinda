{literal}
<script>
(function($, window){
    $(document).ready(function() {
        function scrollTo(oTarget) {
            var alto = 0;

            var ooo = oTarget.offset().top - alto;
            $('html, body').animate({ scrollTop:ooo }, 600);
        }
        
    	$("div.boton_faq").click(function() {
            if ($(this).parent().hasClass('activo')){
                $(".acordeon.activo").removeClass('activo');
            }
            else{
                $(".acordeon.activo").removeClass('activo');
                var $div = $(this).parent();
                $div.addClass('activo');
                setTimeout(function() {
                    scrollTo($div);
                }, 300);
            }
        });
    });
})(jQuery, window);
</script>
{/literal}

{hook h='displayInnerHeader'}
<div class="container">
    <div class="row">
        <div class="col-md-3 col-lg-2">
            {hook h='displayInnerLeftCol'}
        </div>
        <div class="col-md-9 col-lg-10 caja faqs">
            <h1 class="titulo">Preguntas Frecuentes - Kelinda</h1>
            <br />
            
            <div class="acordeon activo">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Dónde pruedo comprar?</strong>
            </div>
            <div>
                <p>Visita nuestros puntos de venta: /<p>
                <p>Si estas en Bogotá:</p>
                <ul>
                    <li>Outlet Punto de fábrica: Av. de las Américas No. 46-17 - Tel.: (+57 1) 337 8145</li>
                    <li>Boutique Punto de fábrica:  Av. de las Américas No. 46-15 - Tel.: (+57 1) 244 4542/li>
                    <li>C.C. Palatino: Cra. 7 No. 139-07 Local 1-05 Tel.: (+57 1) 614 0320</li>
                    <li>C.C. Colina 138: Calle 138 No. 55-53 Local 27 - Tel.: (+57 1) 625 6180</li>
                    <li>C.C. Plaza imperial: Calle 146 No. 106-20 Local 2-101 - Tel.: (+57 1) 697 5881</li>
                    <li>C.C. Centro Mayor: Cra. 27 No. 38 A 83 Sur Local 1-110 - Tel.: (+57 1) 209 8822</li>
                    <li>C.C. Floresta: Av. Cra 68 No. 90-88 Local 2-024 - Tel.: (+57 1) 226 8581</li>
                    <li>C.C. Santafé: Calle 185 No. 45-03 Local 2-50 - Tel.: (+57 1) 487 0144</li>
                    <li>C.C. Unicentro: Cra. 15 No. 123-30 Local 1-133 - Tel.: (+57 1) 215 8009</li>
                    <li>C.C. Outlet Plaza: Cra. 62 No. 9-60 Local 106 A - Tel.: (+57 1) 414 6358</li>
                </ul>
                <p>En Girardot:</p>
                <ul>
                    <li>Centro Comercial Premier: Calle 28 No. 13-30 Local 121  - Tel.: (+57 1) 831 4047</li>
                </ul>
                <p>Si prefieres no salir de casa, realiza tus compras por medio de nuestro portal web <a href="http://kelinda.com">kelinda.com</a></p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cómo puedo realizar mi compra a través de la tienda virtual?</strong>
            </div>
            <div>
                <p>Desarrollamos todo el contenido de tu tienda virtual <a href="http://kelinda.com">kelinda.com</a> pensando en que sea muy accesible 
                    para ti así que comprar es muy sencillo:</p>
                <ol>
                    <li>La bolsa o carrito de compra es una herramienta flotante en forma de globo que te acompañara siempre en tu 
                        recorrido por la tienda virtual, en ella podrás hacer tus compras directas, en el caso de que ya tengas claro 
                        que quieres, o ver lo que has seleccionado cuando vas a hacer compra de varios artículos,
                        te informa cuantos artículos llevas y el valor en pesos de lo que selecciones con tan solo dar click sobre ella.
                    </li>
                    <li>En el Home principal podrás encontrar las distintas categorías en el menú principal 
                        (INICIO - MUJERES – HOMBRES – NIÑOS – CATÁLOGOS – TIENDAS – CONTACTO), 
                        él estará presente en toda la tienda para que puedas acceder directamente a lo que deseas mirar o 
                        simplemente aventurarte a explorar todo lo que <a href="http://kelinda.com">kelinda.com</a> tiene para ti.
                    </li>
                    <li><p>Opción 1: Una vez hayas escogido una categoría (MUJERES, HOMBRES O NIÑOS) podrás ver todas las 
                            referencias disponibles para esa categoría.</p>
                        <p>Opción 2: En el INICIO hay un botón de <<&iexcl;comprar ya!>> con un click te llega directo a ver lo que hay disponible.</p>
                        <p>Opción 3: También en INICIO al bajar podrás ver las novedades que <a href="http://kelinda.com">kelinda.com</a> tiene y al dar click sobre ver más 
                        te mostrar también las referencias disponibles.</p>
                        <p>Opción 4: Pensando en ti hemos creado un menú que te ayudara a elegir prendas según tu figura, cuando elijas 
                        la silueta con la que te identificas, das click sobre esta y ella te llevara a las 
                        referencias disponibles donde con seguridad podrás elegir las que te gusten.</p>
                    </li>
                    <li><p>Al dar click sobre el nombre de una referencia, entras a ver al detalle todo acerca de esta; descripción de la prenda,
                        colores, tallas, siluetas, sugeridos y precios, donde también encontrarás la opción de compra al lado derecho, 
                        allí eliges talla, color y cantidad y al final de todo esto encontraras un botón con una bolsa similar al 
                        globo flotante, que no es más que una réplica de este, es decir un botón de compra, para que tengas una opción 
                        directa de agregar al carrito o comprar la referencia que estabas viendo.</p>
                        <blockquote>
                            * Nota: Recuerda que si no estás registrado(a) en <a href="http://kelinda.com">kelinda.com</a>, al hacer tu compra te pediremos tus datos,
                            que son necesarios para la realización de compras, ya que no es posible venderle o enviarle mercancía a 
                            anónimos por cuestiones legales, así que al registrarte se te creara una sesión para que puedas ingresar 
                            cada vez que desees. Si ya estas registrado(a) deberás iniciar sección.
                        </blockquote>
                    </li>
                    <li>Al Dar click sobre el botón de compra, te dirigirá a una nueva ventana para que puedas revisar los artículos que has 
                        seleccionado, continuar eligiendo más referencias o ir a la caja a pagar. </li>
                    <li>Cuando vas a la caja podrás elegir la forma de pago a través de PayU, en esta parte del proceso es donde si pagas 
                        con tarjeta ingresas los datos de tu tarjeta; también puedes hacerlo Vía Baloto o Efecty, PayU te facilitará un 
                        documento imprimible que debes presentar cuando realices tu pago. Recuerda que PayU te debe confirmar que el pago 
                        fue realizado a través de un mensaje de correo electrónico.</li>
                    <li><p>Finalizado este proceso nosotros procederemos al despacho y envió de tu pedido.</p>
                        <blockquote>*Nota: Antes de tu compra revisa el cuadro de tallas, para que evites pedir la talla incorrecta. 
                            También asegúrate de estar seleccionado el color y las cantidades por unidad que deseas recibir.</blockquote>
                    </li>
                </ol>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cómo puedo elegir la talla correcta de mi vestido de baño o prenda?</strong>
            </div>
            <div>
                <p>En nuestra página encontraras un cuadro de tallas, donde según tu talla de Brassier y Panty te aconsejamos 
                    la talla indicada de nuestro vestido de baño para cada una de las líneas Kelinda</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cómo puedo realizar un cambio por talla?</strong>
            </div>
            <div>
                <p>Para realizar el cambio de prenda por talla, dentro de los 5 días posteriores a la compra con la respectiva factura, 
                    envía la prenda en perfecto estado con sus etiquetas y el formato de cambios y devoluciones, que debes diligenciar y 
                    que encontraras adjunto <a href="{$base_dir}archivos/formato_cambios_y_devoluciones.xlsx">aquí</a>, a la dirección 
                    Av. Américas #46 – 17, Bogotá – Colombia.
                    <br />
                    <br />
                    Véase también: <a href="{$base_dir}content/1-devoluciones-cambios-garantias">Devoluciones, cambios y garantías</a></p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Qué garantía tiene mi vestido de baño Kelinda?</strong>
            </div>
            <div>
                <p>Tu vestido de baño tiene 30 días de garantía contados a partir de la fecha de compra por defectos de fabricación, 
                    no cubre el mal uso, mal lavado o desgaste natural de la prenda.
                    <br />
                    <br />
                    Véase también: <a href="{$base_dir}content/1-devoluciones-cambios-garantias">Devoluciones, cambios y garantías</a></p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cómo hago efectiva la garantía de mi producto?</strong>
            </div>
            <div>
                <p>Si está dentro de los 30 días de compra, enviara a nuestras oficinas ubicada en la Av. Américas N. 46 – 17 
                    (Puente Aranda) Bogotá. La prenda debe estar limpia, se debe adjuntar la factura de compra con el formato de c
                    ambios y devoluciones que encontraras adjunto <a href="{$base_dir}archivos/formato_cambios_y_devoluciones.xlsx">aquí</a>.
                    La respuesta de tu vestido te la daremos dentro de 
                    los 15 días hábiles siguientes a la fecha de envío, ya sea la prenda reparada o una nueva según lo analizado 
                    en el departamento de calidad. No hacemos devoluciones de dinero, si es autorizado el envío de una prenda 
                    nueva deberás escoger una de igual o mayor valor y pagar la diferencia.
                    <br />
                    <br />
                    Véase también: <a href="{$base_dir}content/1-devoluciones-cambios-garantias">Devoluciones, cambios y garantías</a></p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cómo se cuál es mi vestido indicado según mi figura?</strong>
            </div>
            <div>
                <p>Tu tienda virtual <a href="http://kelinda.com">kelinda.com</a> te ofrece la opción de selección de siluetas, encontraras una descripción 
                    de cada silueta con la cual podrás identificarte, te sugerimos que diferentes referencias que tenemos 
                    disponibles y que se ajusta a tu figura.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Qué debo hacer si cuando realizo mi compra la página se me bloquea y no me deja finalizar?</strong>
            </div>
            <div>
                <p>Por favor comunícate al correo electrónico <a href="mailto:kelinda@kelinda.com">kelinda@kelinda.com</a> para que solicites la cancelación de tu 
                    pedido y así puedas volver a intentarlo.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cuáles son los medios de pago para realizar una compra por medio de la página?</strong>
            </div>
            <div>
                <p>Los pagos se reciben a través de nuestro proveerdor de pagos online PayU, así que tenemos disponibles 
                    pago con tarjetas crédito o débito (Visa, American Express, Diners Club, MasterCard y Pse), 
                    pago en efectivo vía Baloto y efecty, ellos se encargaran de confirmarte si tu pago fue realizado. 
                    No somos responsables de errores de digitación por parte de PayU, ellos directamente se encargaran.</p>
                <blockquote>*No tenemos disponible el pago contra entrega.</blockquote>
                <p>Para el pago en efectivo vía Baloto y efecty, el valor mínimo es de $20.000 pesos y se debe hacer en un 
                    plazo de 48 horas, de no ser así el pedido será cancelado.</p>
                <p>Una vez tu pedido sea confirmado debes imprimir el comprobante que presentaras en efecty o Baloto 
                    para cancelar el monto.</p>
                <p>Puedes consultar tu transacción en la página de PayU en la sección de compradores.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Qué hago si mi pago es rechazado o no ha sido notificado?</strong>
            </div>
            <div>
                <p>Si tu tarjeta fue debitada, puede haberse presentado algún problema de comunicación con las entidades 
                    bancarias y PayU, en este caso debes enviar una copia del soporte donde se muestre el débito a PayU 
                    para que ellos se encargue de hacer los ajustes pertinentes, si hay devolución del dinero el trámite 
                    puede tardar 30 días hábiles.</p>
                <p>Si tu tarjeta no fue debitada comunícate directamente con tu entidad bancaria o a la sección de servicio 
                    al cliente de PayU, ellos te responderán.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Como puedo verificar el estado de mi pedido?</strong>
            </div>
            <div>
                <p>Al terminar de realizar tu compra, nuestro portal te informará el costo del envío, monto a pagar y 
                    número de guía para que puedas rastrear y verificar el despacho de la mercancía a través <a href="http://coordinadora.com">coordinadora.com</a>. 
                    Coordinadora es la transportadora autorizada por Creaciones Kelinda Limitada para el envío de tus pedidos. </p>
                <p>Cualquier inquietud podrás comunicarte a la dirección de correo electrónico <a href="mailto:kelinda@kelinda.com">kelinda@kelinda.com</a></p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Qué pasa si no estoy presente cuando llegue mi pedido?</strong>
            </div>
            <div>
                <p>Si no estar presente a la momento de la entrega, Coordinadora realiza tres intentos de entrega en distintos horarios, 
                    en caso de que en el tercer intento tampoco se concrete la entrega, la transportadora nos informará, para que nos 
                    comuniquemos contigo y así verificar la dirección; si no hay comunicación, se autorizara a coordinadora la devolución 
                    del paquete. Las entregas solo pueden ser recibidas por personas mayores de edad y en condición apta para recibir el 
                    paquete.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Hacen envíos al extranjero?</strong>
            </div>
            <div>
                <p>Por el momento no tenemos habilidad esta opción, sin embargo, podemos indicarte donde puedes encontrar 
                    nuestros producto en otros países; puedes comunicarte con nuestra área comercial, al correo electrónico 
                    <a href="mailto:gcomercial@kelinda.com">gcomercial@kelinda.com</a> o <a href="mailto:kelinda@kelinda.com">kelinda@kelinda.com</a>.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cuánto tiempo tarda mi pedido en llegar?</strong>
            </div>
            <div>
                <p>El plazo de entrega es 10 días hábiles, sin embargo pueden presentarse alteraciones por las cuales no 
                    se pueda cumplir, pero esta no se podrá extender a más de 30 días. Creaciones Kelinda Limitada 
                    se comunicará contigo para informarte alguna novedad sobre tu pedido, si es necesario; en caso de no 
                    ser posible la entrega bajo el término de 30 días, se procederá a la devolución de tu dinero a través 
                    de PayU y se te informará a tu correo electrónico, como lo estipula la 
                    <a href="http://www.alcaldiabogota.gov.co/sisjur/normas/Norma1.jsp?i=44306">Ley 1480 de 2011, articulo 49</a></p>
                <blockquote>* Recuerda que no contamos con pago contra entrega.</blockquote>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Por qué no puedo comprar algunas de las referencias que aparecen?</strong>
            </div>
            <div>
                <p>Es posible que no hallan existencias, sin embargo puedes comunicarte con nosotros y solicitar información 
                    sobre la disponibilidad del producto al PBX 3378055, Fax 2686379, teléfono móvil 3102226214, en el horario 
                    de atención Lunes a Viernes de 8:00am a 12:30pm y 2:00pm a 4:30pm; o al correo electrónico <a href="mailto:kelinda@kelinda.com">kelinda@kelinda.com</a>.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>Deseo hacer compras al por mayor, ¿qué debo hacer?</strong>
            </div>
            <div>
                <p>Te invitamos a  comunicarte con nuestra área comercial, al correo electrónico <a href="mailto:gcomercial@kelinda.com">gcomercial@kelinda.com</a> 
                    te indicaran como hacer tu compra.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Qué hago para distribuir Kelinda en el país donde me encuentro?</strong>
            </div>
            <div>
                <p>Para información sobre cómo ser distribuidores de nuestros productos en otras partes del mundo, 
                    puedes comunicarte con nuestra área comercial, al correo electrónico <a href="mailto:gcomercial@kelinda.com">gcomercial@kelinda.com</a> o 
                    <a href="mailto:kelinda@kelinda.com">kelinda@kelinda.com</a>.</p>
            </div>
        </div>
    </div>
</div>


