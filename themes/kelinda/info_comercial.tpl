{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{hook h='displayInnerHeader'}
<div class="container">
    <div class="row">
        <div class="col-md-3 col-lg-2 col-lg-2">
            {hook h='displayInnerLeftCol'}
        </div>
        <div class="col-md-9 col-lg-10 col-lg-10 caja gris">
            <br />
            <h1 class="page-heading">
                <img src="{$base_dir}themes/kelinda/imagenes/icono_kelinda_black.png" />
                Informaci&oacute;n comercial
            </h1>
            <div class="row">
                <div class="col-md-4">
                    <strong>FABRICA Y OFICINAS BOGOT&Aacute;</strong><br />
                    Av. de las Am&eacute;ricas No. 46-17<br />
                    mail: kelinda@kelinda.com<br />
                    Tel.: (+57 1) 337 8055<br />
                    Fax: (+57 1) 268 6379<br />
                    Cel: : (+57) 310 222 6214
                </div>
                <div class="col-md-4">
                    <strong>GERENCIA COMERCIAL NACIONAL E INTERNACIONAL</strong><br />
                    mail: gcomercial@kelinda.com<br />
                    Cel.: (+57) 304 3752187<br />
                    Tel: (+57 1) 3378055<br />
                </div>
                <div class="col-md-4">
                    <strong>VENTAS BOGOT&Aacute;</strong><br />
                    mail: mppinzon@kelinda.com<br />
                    Cel.: 310 254 7978<br />
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <strong>VENTAS COSTA ATL&Aacute;NTICA</strong><br />
                    mail: cherrera@kelinda.com<br />
                    Cel.: 311 664 6469<br />
                </div>
                <div class="col-md-4">
                    <strong>VENTAS TOLIMA, HUILA CAQUET&Aacute;</strong><br />
                    mail: hbustos@kelinda.com<br />
                    Cel.: 312 572 8706<br />
                </div>
                <div class="col-md-4">
                    <strong>VENTAS SANTANDERES, BOYAC&Aacute; Y LLANOS ORIENTALES</strong><br />
                    mail: ventascentral@kelinda.com<br />
                    Cel.: 314 394 3033<br />
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <strong><big>DISTRIBUIDORES ECUADOR</big></strong><br />
                    D' Luna Distribucioes S.A.<br />
                    Tel: (5934) 2655021 / (5934) 2656401<br />
                    E-Mail: ldelgado@dluna.com.ec<br />
                    Garzota 2 Manzana 44 Solar 29<br />
                    Guayaquil, Ecuador
                </div>
                <div class="col-md-4">

                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>
    </div>
</div>
