<script>
    (function ($, window) {
        $(document).ready(function () {
            $("div.boton_faq").click(function () {
                if ($(this).parent().hasClass('activo')) {
                    $(".acordeon.activo").removeClass('activo');
                } else {
                    $(".acordeon.activo").removeClass('activo');
                    $(this).parent().addClass('activo');
                }
            });
        });
    })(jQuery, window);
</script>
{hook h='displayInnerHeader'}
{capture name=path}Bolsa de empleo{/capture}
<div class="container">
    <div class="row">
        <div class="col-md-3 col-lg-2">
            {hook h='displayInnerLeftCol'}
        </div>
        <div class="col-md-9 col-lg-10 caja gris">
            <h1 class="page-heading">
                <img src="{$base_dir}themes/kelinda/imagenes/icono_kelinda_black.png" />
                Bolsa de empleo
            </h1>

            {if (!empty($respuesta_tarea))}
                <h2 class="titulo">{$respuesta_tarea}</h2>
            {/if}
            <div class="caja blanca">
                <!--p>Revise nuestras vacantes activas y sí está interesado en aplicar a alguna, por favor envi&eacute;nos su hoja de vida.</p>
                <br /-->
                <p>Por favor descargue el formato de hoja de vida haciendo clic <a class="sub" href="{$base_dir}archivos/formato_hoja_de_vida.xlsx">aqu&iacute;</a>,
                    luego env&iacute;elo diligenciando el siguiente formulario y nos pondremos en contacto con usted.</p>
                <p>Los campos marcados con asterisco (*) son requeridos. </p>
                <br />
                <form method="post" class="form" action="{$base_dir}/index.php?controller=custom&tarea=enviar_hoja_vida" enctype="multipart/form-data">
                    <input type="hidden" name="destino" id="destino" value="{$destino}" />
                    <input type="hidden" name="vista" id="vista" value="respuesta_form" />
                    <div class="row form-group">
                        <div class="col-md-2">
                            <label for="nombre" required>* Nombre</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="nombre" id="nombre" required />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-2">
                            <label for="telefono">Tel&eacute;fono</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" name="telefono" id="telefono" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-2">
                            <label for="email" required>* Email</label>
                        </div>
                        <div class="col-md-10">
                            <input type="email" name="email" id="email" required />
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-2">
                            <label for="asunto">* Adjunte hoja de vida</label>
                        </div>
                        <div class="col-md-10">
                            <input type="file" name="archivo" id="archivo" />
                        </div>
                    </div>

                    <div class="row form-group">
                        <input type="submit" class="boton std gris right" value="Enviar" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>