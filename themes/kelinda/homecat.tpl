{hook h='displayInnerHeader' home="0"}
<div class="container">
    <div class="col-md-3 col-lg-2">
        {if $categorias_menu|@count > 0}
            <a class="menu-resp home" href="#" data-menu="lista_cat">Categor&iacute;as</a>
            <div id="lista_cat">
            {include file="./categorias-arbol.tpl" cats=$categorias_menu}
            </div>
        {/if}
        
    </div>
    <div class="col-md-9 col-lg-10">
        {foreach from=$categorias_home item=categoria}
        <div class="row separador">
            <div class="col-sm-12" style="background:url({$link->getCatImageLink($categoria->link_rewrite, $categoria->id_image, 'k_cat_std')|escape:'html':'UTF-8'}) left top no-repeat; background-size:cover">
                <div class="contenedor_categoria_home">
                    <div class="row">
                        <div class="col-md-6 pre-info relativo">
                            <a class="ver-mas abs" href="{$link->getCategoryLink($categoria->id_category, $categoria->link_rewrite)|escape:'html':'UTF-8'}">Ver m&aacute;s</a>
                        </div>
                        <div class="col-md-6 info">
                            {$categoria->description}
                        </div>
                    </div>
                </div>
            </div>        
        </div>
        {/foreach}
    </div>
</div>