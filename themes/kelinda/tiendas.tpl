<script>
    (function ($, window) {
        function adicionarMarcador(mapa, lat, lng, info, img) {
            var latlng = new google.maps.LatLng(lat, lng);
            var marker = new google.maps.Marker({
                position: latlng,
                map: mapa,
                html: info,
                icon: img,
                animation: google.maps.Animation.DROP,
                draggable: true
            });

            var infoWindow = new google.maps.InfoWindow({
                content: marker.html
            });
            infoWindow.open(mapa, marker);

            google.maps.event.addListener(marker, "click", function () {
                infoWindow.setContent(this.html);
                infoWindow.open(mapa, this);
            });

            return marker;
        }

        $(document).ready(function () {
            var options = {
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("mapa"), options);

            $("a[rel=mapa]").click(function (e) {
                e.preventDefault();
                var $lat = $(this).attr("data-lat");
                var $lng = $(this).attr("data-lng");
                var latlng = new google.maps.LatLng($lat, $lng);
                map.setZoom(16);
                map.setCenter(latlng);
            });

    {foreach from=$tiendas item=tienda}
            var marker = adicionarMarcador(map, {$tienda.latitud}, {$tienda.longitud}, '{$tienda.nombre} {$tienda.direccion}<br />Tel. {$tienda.telefono}', undefined);

        {if $tienda.centro =='S'}
                        map.setCenter(marker.getPosition());
        {/if}
    {/foreach}
                    });
                })(jQuery, window);
</script>

{hook h='displayInnerHeader'}
<div class="container">
    <div class="row">
        <div class="col-md-3 col-lg-2 col-lg-2">
            {hook h='displayInnerLeftCol'}
        </div>
        <div class="col-md-9 col-lg-10 col-lg-10 caja gris">
            <br />
            <h3 class="page-subheading big">Tiendas</h3>
            <ul class="row">
                {foreach from=$tiendas item=tienda}
                    <li class="col-md-6">
                        <a href="#" rel="mapa" data-lat="{$tienda.latitud}" data-lng="{$tienda.longitud}">
                            <i class="icon-map-marker"></i>&nbsp;{$tienda.nombre} {$tienda.direccion} - Tel. {$tienda.telefono}
                        </a>
                    </li>
                {/foreach}
            </ul>
            <div id="mapa" style="display: block; width: 100%; height: 100%; min-height: 400px; margin: 20px 0;"></div>
        </div>
    </div>
</div>
