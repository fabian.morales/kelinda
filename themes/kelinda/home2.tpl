<div class="container">
    <div class="row separador">
        <div class="col-md-1 uppercase"><a href="{$base_dir}"><img src="{$base_dir}themes/kelinda/imagenes/icono_kelinda_black.png" class="img-responsive" /></a></div>
        <div class="col-md-5 uppercase">            
            {if $logged}
                <div class="usuario home black">
                    <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="Mi cuenta" class="account left" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>
                    <span class="left">&nbsp;-&nbsp;</span>
                    <a href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" title="Logout" rel="nofollow" class="left">Logout</a>
                </div>
            {else}
                <form action="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" method="post" id="login_form_home1" class="black">
                    <div class="row">
                        <div class="col-md-4 usuario home black">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-md-8">
                                    <input class="is_required validate account_input form-control" data-validate="isEmail" type="email" id="email" name="email" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">&nbsp;</div>
                        <div class="col-md-4 usuario home black">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="passwd">Clave</label>
                                </div>
                                <div class="col-md-8">
                                    <input class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="passwd" name="passwd" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">&nbsp;</div>
                        <div class="col-md-2">
                            {if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'html':'UTF-8'}" />{/if}
                            <button type="submit" id="SubmitLogin" name="SubmitLogin">
                                Entrar
                            </button>
                        </div>
                    </div>
                </form>
                {*<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html'}" rel="nofollow" title="Login">Login / Reg&iacute;strate</a>*}
            {/if}
        </div>
        <div class="col-md-3 uppercase">
            {if !$logged}
                <a class="login header" href="{$link->getPageLink('my-account', true)|escape:'html'}" rel="nofollow" title="Registrarse">Reg&iacute;strate / Olvid&eacute; mi contrase&ntilde;a</a>
            {else}
                &nbsp;
            {/if}
        </div>
        <div class="col-md-3">
            <a href="#" class="boton carrito text-center"><img src="{$base_dir}themes/kelinda/imagenes/icono_carrito_black.png" /> Mi carrito de compras</a>
        </div>
    </div>
</div>

{hook h='displayInnerHeader' home="1"}
<div class="container">
    <div class="row separador">
        <div class="col-md-6 relativo">
            <img src="{$base_dir}themes/kelinda/imagenes/categoria1_home2.jpg" class="img-responsive img-fullw" />
            <a class="ver-mas abs" href="http://andresmesa.co/kelinda/12-Array">Ver m&aacute;s</a>
        </div>
        <div class="col-md-6 relativo">
            <img src="{$base_dir}themes/kelinda/imagenes/categoria2_home2.jpg" class="img-responsive img-fullw" />
            <a class="ver-mas abs" href="http://andresmesa.co/kelinda/14-Array">Ver m&aacute;s</a>
        </div>
    </div>
    <div class="row separador">
        <div class="col-md-9">
            <img src="{$base_dir}themes/kelinda/imagenes/descubre_tu_silueta.jpg" class="img-responsive img-fullw" />
        </div>
        <div class="col-md-3">
            <ul id="cat_siluetas_home">
                {foreach from=$categorias_sil item=cat}
                    <li data-categoria="gal_cat_{$cat->id_category}">
                        <a href="{$link->getCategoryLink($cat->id_category, $cat->link_rewrite)|escape:'html':'UTF-8'}" title="{$cat->name|escape:'html':'UTF-8'}">                
                            <img src="{$link->getCatImageLink($cat->link_rewrite, $cat->id_image, 'k_cat_sil_std')|escape:'html':'UTF-8'}" alt="{$cat->name|escape:'html':'UTF-8'}" />
                        </a>
                    </li>
                {/foreach}
            </ul>
        </div>
    </div>
    <div class="row separador">
        <div class="col-md-12">
            <img src="{$base_dir}themes/kelinda/imagenes/kelinda_nueva_coleccion.jpg" class="img-responsive img-fullw" />
        </div>
    </div>
</div>