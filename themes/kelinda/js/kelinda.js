(function ($, window) {
    function obtenerCiudades() {
        $.ajax({
            url: 'index.php?controller=custom&tarea=obtener_ciudades',
            data: {id_depto: $("#id_state").val()},
            method: 'post',
            dataType: 'json',
            success: function (json) {
                $("#city").val('');
                $("#city").html('');

                $.each(json, function (i, o) {
                    var $selected = '';

                    if ((o.codigo + '-' + o.nombre) === $("#city_old").val()) {
                        $selected = ' selected="selected"';
                    }

                    $("#city").append('<option value="' + o.codigo + '-' + o.nombre + '"' + $selected + '>' + o.codigo + '-' + o.nombre + '</option>');

                    if ((o.codigo + '-' + o.nombre) === $("#city_old").val()) {
                        $("#city").val(o.codigo + '-' + o.nombre);
                    }

                    $.uniform.update("#city");
                });
            }
        });
    }

    $(document).ready(function () {
        $(window).resize(function() {
            $(".menu-resp").each(function(i, o) {
                var menu = "#" + $(o).attr("data-menu");
                if ($(o).is(":visible")){
                    $(menu).hide();
                }
                else{                    
                    $(menu).show();
                }                
            });

        });
        
        $("h1.menu-footer").click(function (e) {
            e.preventDefault();
            $(this).parent().find("ul.menu-footer").toggle('slow');
        });

        $("#cat_siluetas_rel").lightSlider({
            item: 3,
            pager: false,
            controls: true
        });

        $("#cat_siluetas_home").lightSlider({
            item: 1,
            auto: true,
            pager: false,
            controls: true
        });

        //$("a[rel=ajax_prod]").fancybox({type: 'ajax', autoSize: true, autoHeight: true, autoWidth: true});
        $("a[rel=ajax_prod]").click(function (e) {
            e.preventDefault();
            $.fancybox({href: $(this).attr("data-url"), type: 'ajax', autoSize: false, maxWidth: '500px', scrolling: 'no', arrows: false, wrapCSS: 'my-fancy', title: null});
        });

        $("#form_filtros select").change(function (e) {
            e.preventDefault();
            $("#form_filtros").submit();
        });

        $("a[rel=img_gal_zoom]").fancybox();

        $("form#add_address #id_state").change(function (e) {
            e.preventDefault();
            obtenerCiudades();
        });

        obtenerCiudades();

        $("img").on({
            "contextmenu": function (e) {
                e.preventDefault();
            }
        });
        
        $(".menu-resp").click(function(e) {
            e.preventDefault();
            var menu = "#" + $(this).attr("data-menu");
            $(menu).toggle("slow");
        });

        /*if (typeof($.jqzoom) != 'undefined'){
         $('a[rel=img_gal_zoom]').jqzoom({
         zoomType: 'innerzoom', //innerzoom/standard/reverse/drag
         zoomWidth: 100, //zooming div default width(default width value is 200)
         zoomHeight: 100, //zooming div default width(default height value is 200)
         xOffset: 10, //zooming div default offset(default offset value is 10)
         yOffset: 0,
         title: false
         });
         }*/
    });
})(jQuery, window);

