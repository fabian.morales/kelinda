{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo header -->
<div class="row">
    <div class="col-md-9">        
        <ul class="menu black right">
            <li><a href="http://pandorascode.co/">Home</a></li>
            <li><a href="http://pandorascode.co/tienda/3-Mujer">Shop</a></li>
            <li><a href="http://pandorascode.co/index.php/lookbook">Lookbook</a></li>
            <li><a href="http://pandorascode.co/index.php/prensa">Prensa</a></li>
            <li><a href="http://pandorascode.co/index.php/component/my_component/?controller=blog&Itemid=105">Blog</a></li>
            <li><a href="http://pandorascode.co/index.php/contacto">Contacto</a></li>
        </ul>
    </div>
    <div class="col-md-3">
        <ul class="redes black">
            <li><a href="https://www.facebook.com/pandora.code/about" target="_blank" class="red facebook">&nbsp;</a></li>
            <li><a href="http://instagram.com/pandorascode" target="_blank" class="red instagram">&nbsp;</a></li>
            <li><a href="https://twitter.com/pandoras_code" target="_blank" class="red twitter">&nbsp;</a></li>
        </ul>
    </div>
</div>