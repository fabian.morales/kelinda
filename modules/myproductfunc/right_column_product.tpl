{if $productos|@count > 0}

    {literal}
    <script>
        (function($, window){
            $(document).ready(function() {           
                $("#cat_siluetas_rel > li")
                .mouseover(function() {
                    var $dataCat = $(this).attr("data-categoria");
                    var $div = $("#"+$dataCat);

                    $div.removeClass("hidden");
                })
                .mouseout(function() {
                    var $dataCat = $(this).attr("data-categoria");
                    var $div = $("#"+$dataCat);
                    $div.addClass("hidden");
                });            
            });
        })(jQuery, window);
    </script>
    {/literal}

    <div style="position: relative" class="tu_silueta">
        <div class="col-sm-12">
            <label class="attribute_label text-center">Tu silueta</label>
            <hr />
            <ul id="cat_siluetas_rel1" class="row no-gutter galeria siluetas">
                {foreach from=$productos key=i item=prodCat}
                    <li data-categoria="gal_cat_{$prodCat.categoria->id_category}" class="col-md-4">
                        <a href="{$link->getCategoryLink($prodCat.categoria->id_category, $prodCat.categoria->link_rewrite)|escape:'html':'UTF-8'}" title="{$prodCat.categoria->name|escape:'html':'UTF-8'}">                
                            <img class="img-responsive img-fullw" src="{$link->getCatImageLink($prodCat.categoria->link_rewrite, $prodCat.categoria->id_image, 'k_cat_sil')|escape:'html':'UTF-8'}" alt="{$prodCat.categoria->name|escape:'html':'UTF-8'}" />
                            <img src="{$base_dir}themes/kelinda/imagenes/kelinda_recomienda.png" class="img-responsive img-fullw" />
                        </a>

                        <div class="contenedor galeria siluetas">
                            <div class="galeria siluetas producto">        
                                <div id="gal_cat_{$i}" class="row">
                                    {if isset($accesorios) && $accesorios}
                                    <div class="col-sm-12">
                                        <h1 class="page-subheading text-center">Recomendados</h1>
                                        <div class="row">
                                            {foreach from=$accesorios item=producto}
                                                <div class="col-md-4 text-center">
                                                   <img src="{$link->getImageLink($producto.link_rewrite, $producto.id_image, 'k_std')|escape:'html':'UTF-8'}" class="img-responsive" />
                                                   <a class="ver-mas mini autocentro" href="{$link->getProductLink($producto)|escape:'html':'UTF-8'}">Ver m&aacute;s</a>
                                                </div>
                                           {/foreach}
                                        </div>
                                    </div>
                                    {/if}
                                    <div class="col-sm-12">
                                        <h1 class="page-subheading text-center">De acuerdo a tu silueta</h1>
                                        <div class="row">
                                            <div class="col-md-3 text-center">
                                                <img class="img-responsive  img-fullw" src="{$link->getCatImageLink($prodCat.categoria->link_rewrite, $prodCat.categoria->id_image, 'k_cat_sil')|escape:'html':'UTF-8'}" alt="{$prodCat.categoria->name|escape:'html':'UTF-8'}" />
                                            </div>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    {foreach from=$prodCat.productos item=producto}
                                                       <div class="col-md-4 text-center">
                                                           <img src="{$link->getImageLink($producto.link_rewrite, $producto.id_image, 'k_std')|escape:'html':'UTF-8'}" class="img-responsive" />
                                                           <a class="ver-mas mini autocentro" href="{$link->getProductLink($producto)|escape:'html':'UTF-8'}">Ver m&aacute;s</a>
                                                       </div>
                                                   {/foreach}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                {/foreach}
            </ul>
        </div>
    </div>
{/if}

