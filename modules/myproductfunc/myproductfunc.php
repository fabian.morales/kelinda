<?php

/*
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2014 PrestaShop SA

 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_CAN_LOAD_FILES_'))
    exit;

class MyProductFunc extends Module {

    public function __construct() {
        $this->name = 'myproductfunc';
        if (version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
            $this->tab = 'front_office_features';
        else
            $this->tab = 'Blocks';
        $this->version = '2.1.1';
        $this->author = 'Fabian Morales';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = 'My Product Func';
        $this->description = 'Informacion y botones funcionales de productos';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install() {
        return parent::install() &&
                $this->registerHook('displayProductListFunctionalButtons') &&
                $this->registerHook('displayProductHover') &&
                $this->registerHook('displayProductExtraRight') &&
                $this->registerHook('header');
    }

    public function uninstall() {
        // Delete configuration
        parent::uninstall();
    }

    public function hookDisplayHeader() {
        $this->context->controller->addCSS(($this->_path) . 'myproductfunc.css', 'all');
        $this->context->controller->addJS(($this->_path) . 'myproductfunc.js');
    }

    public function hookDisplayProductListFunctionalButtons($params) {
        $this->smarty->assign('producto', $params['product']);
        //$this->context->controller->addCSS($this->_path.'style.css', 'all');
        return $this->display(__FILE__, 'myproductfunc.tpl');
    }

    public function hookDisplayProductHover($params) {
        $this->smarty->assign('producto', $params['product']);
        //$this->context->controller->addCSS($this->_path.'style.css', 'all');
        return $this->display(__FILE__, 'hover.tpl');
    }

    public function hookDisplayInnerLeftCol($params) {
        return $this->hookDisplayLeftColumnProduct($params);
    }

    public function hookDisplayLeftColumnProduct($params) {
        $this->smarty->assign('cats', $this->obtenerCategorias());
        //$this->context->controller->addCSS($this->_path.'style.css', 'all');
        return $this->display(__FILE__, 'left_column_product.tpl');
    }

    public function hookDisplayRightColumnProduct($params) {
        $cats = Category::getCategories((int) ($this->context->language->id), true, false, ' and c.id_parent = 21');

        $categorias = array();
        $productos = array();
        $accesorios = array();
        foreach ($cats as $c) {
            $_c = New Category((int) $c["id_category"], (int) ($this->context->language->id));
            $categorias[] = $_c;
            $productos[$c["id_category"]]["categoria"] = $_c;
            $productos[$c["id_category"]]["productos"] = $_c->getProducts((int) ($this->context->language->id), 1, 20);
        }

        if (!empty($params['product'])) {
            //$producto = new Product((int) $params['product']->id, (int) ($this->context->language->id));
            //if (sizeof($producto)) {
            $accesorios = $params['product']->getAccessories((int) ($this->context->language->id));
            //}
        }

        $this->smarty->assign('categorias', $categorias);
        $this->smarty->assign('productos', $productos);
        $this->smarty->assign('accesorios', $accesorios);

        return $this->display(__FILE__, 'right_column_product.tpl');
    }

    public function hookDisplayProductExtraRight($params) {
        return $this->hookDisplayRightColumnProduct($params);
    }

    public function hookDisplayProductTalla($params) {
        $imagenes = array();
        if (!empty($params['product'])) {
            $guias = array(
                12 => "guia-kelinda-holiday-mujer.jpg",
                13 => "guia-kelinda-elite.jpg",
                14 => "guia-sunday.jpg",
                33 => "guia-kelinda-holiday-mujer.jpg",
                49 => "guia-holiday-hombre.jpg",
                50 => "guia-delfin.jpg",
                51 => "guia-kelinda-kids.jpg",
                52 => "",
                53 => "guia-holiday-niño.jpg",
                54 => "guia-kelinda-kids.jpg",
            );

            $categorias = Product::getProductCategories((int) $params['product']->id);

            foreach ($guias as $i => $g) {
                if (in_array($i, $categorias)) {
                    $imagenes[] = $g;
                }
            }

            $this->smarty->assign('guias', $imagenes);

            return $this->display(__FILE__, 'talla_product.tpl');
        }
    }

    public function obtenerCategorias($padre) {
        $categorias = null;
        $res = array();
        if (!sizeof($padre)) {
            $categorias = Category::getCategories((int) ($this->context->language->id), true, false, ' and c.level_depth = 2', ' order by category_shop.position asc');
        } else {
            $categorias = Category::getChildren($padre["id_category"], (int) ($this->context->language->id), true);
        }

        foreach ($categorias as $c) {
            $c["hijas"] = $this->obtenerCategorias($c);
            $res[] = $c;
        }

        return $res;
    }

}
