{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="vista_rapida">
    <a href="{$producto.link|escape:'html':'UTF-8'}" rel="ajax_prod" data-url="{$base_dir}/index.php?controller=custom&tarea=producto_ajax&id_producto={$producto.id_product}">
        <img src="{$base_dir}/themes/kelinda/imagenes/vista_rapida.png" />
    </a>
</div>
<div class="product hover">
    <div class="row">
        <div class="col-xs-4">
            <div class="mas">
                <a href="{$link->getPageLink('cart')}?qty=1&amp;id_product={$producto.id_product}&amp;token={$static_token}&amp;add">
                    <img src="{$base_dir}/themes/kelinda/imagenes/mas.png" />
                </a>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="ver-mas-quickview">
                <a href="{$producto.link|escape:'html':'UTF-8'}">
                    <img src="{$base_dir}/themes/kelinda/imagenes/ver_mas.png" />
                </a>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="favorito">
                <a class="addToWishlist wishlistProd_{$producto.id_product|intval}" href="#" rel="{$producto.id_product|intval}" onclick="WishlistCart('wishlist_block_list', 'add', '{$producto.id_product|intval}', false, 1);
                        return false;">
                    <img src="{$base_dir}/themes/kelinda/imagenes/favorito.png" />
                </a>
            </div>
        </div>
    </div>
</div>