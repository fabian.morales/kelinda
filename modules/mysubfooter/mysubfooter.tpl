{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo sub-footer -->
{if $page_name == 'index'}
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <ul class="redes black">
                        <li><a href="https://www.facebook.com/KelindaSwimwear/" class="facebook" target="_blank">&nbsp;</a></li>
                        <li><a href="https://www.instagram.com/kelindaswimwear/" class="instagram" target="_blank">&nbsp;</a></li>
                        <li><a href="https://twitter.com/KelindaSwimwear " class="twitter" target="_blank">&nbsp;</a></li>
                    </ul>
                </div>
                <div class="col-md-2 uppercase"><a href="#">Acerca de Kelinda</a></div>
                <div class="col-md-2 uppercase"><a href="#">Suscr&iacute;bete</a></div>
                <div class="col-md-4 uppercase">
                    {if $logged}
                        <div class="usuario home">
                            <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="Mi cuenta" class="account left" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>
                            <span class="left">&nbsp;-&nbsp;</span>
                            <a href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" title="Logout" rel="nofollow" class="left">Logout</a>
                        </div>
                    {else}
                        <form action="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" method="post" id="login_form_home1">
                            <div class="row">
                                <div class="col-md-4 usuario home">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="email">Email</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="is_required validate account_input form-control" data-validate="isEmail" type="email" id="email" name="email" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-4 usuario home">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="passwd">Clave</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="is_required validate account_input form-control" type="password" data-validate="isPasswd" id="passwd" name="passwd" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">&nbsp;</div>
                                <div class="col-md-2">
                                    {if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'html':'UTF-8'}" />{/if}
                                    <button type="submit" id="SubmitLogin" name="SubmitLogin">
                                        Entrar
                                    </button>
                                </div>
                            </div>
                        </form>
                        {*<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html'}" rel="nofollow" title="Login">Login</a>*}
                    {/if}
                </div>
                <div class="col-md-2 uppercase"><img src="{$base_dir}themes/kelinda/imagenes/logo_mini.png" /></div>
            </div>
        </div>
    </footer>
{else}
    <div class="container">
        <div class="row separador">
            <div class="col-md-4">
                <h1 class="menu-footer">Menu</h1>
                <ul class="menu-footer">
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=3">Mujeres</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=45">Hombres</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=47">Ni&ntilde;as</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=46">Ni&ntilde;os</a></li>
                </ul>
            </div>

            <div class="col-md-4">
                <h1 class="menu-footer">Institucional</h1>
                <ul class="menu-footer">
                    <li><a href="{$base_dir}content/5-acerca-de-kelinda">Creaciones Kelinda Limitada</a></li>
                    <li><a href="{$base_dir}content/7-vision">Visi&oacute;n</a></li>
                    <li><a href="{$base_dir}content/6-mision">Misi&oacute;n</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=info_comercial">Informaci&oacute;n comercial</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=catalogo">Portafolio de productos</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=mostrar_hoja_vida">Trabaja con nosotros</a></li>
                </ul>
            </div>

            <div class="col-md-4">
                <h1 class="menu-footer">Servicio al cliente</h1>
                <ul class="menu-footer">
                    {if !$logged}
                        <li><a href="{$link->getPageLink('my-account', true)|escape:'html'}">Reg&iacute;strate</a></li>
                        {/if}
                    <li><a href="{$base_dir}content/4-cuidado-de-tus-prendas">Cuidado de tus prendas</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=tiendas">Tiendas</a></li>
                    <li><a href="{$base_dir}content/2-politicas-privacidad">Pol&iacute;ticas de privacidad</a></li>
                    <li><a href="{$base_dir}content/3-terminos-y-condiciones-de-uso">T&eacute;rminos y condiciones</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=preguntas_frecuentes">Preguntas frecuentes</a></li>
                    <li><a href="{$base_dir}content/1-devoluciones-cambios-garantias">Garant&iacute;as</a></li>
                    <li><a href="{$base_dir}index.php?controller=custom&tarea=mostrarContacto">Contacto</a></li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <a href="{$base_dir}index.php?controller=custom&tarea=tiendas">
                    <img src="{$base_dir}themes/kelinda/imagenes/tiendas.jpg" class="img-fullw img-responsive" />
                </a>
            </div>
            <div class="col-md-4 relativo">
                <a href="https://www.instagram.com/kelindaswimwear/" target="_blank">
                    <img src="{$base_dir}themes/kelinda/imagenes/compartir.png" class="img-fullw img-responsive" />
                </a>
                <div class="compartir abs">
                    <div class="productinfo compartir">Compartir</div>
                    <div class="productinfo">
                        <ul class="redes white">
                            <li><a href="https://www.facebook.com/KelindaSwimwear/" class="red facebook" target="_blank"></a></li>
                            <li><a href="https://www.instagram.com/kelindaswimwear/" class="red instagram" target="_blank"></a></li>
                            <li><a href="https://twitter.com/KelindaSwimwear " class="red twitter" target="_blank"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer id="footer">
        <div class="container">
            <div class="col-sm-12 info_footer">
                <span>&copy; 2012 Creaciones <img src="{$base_dir}themes/kelinda/imagenes/logo_mini.png" /></span>
                <span>Limitada Tel&eacute;fono (+57 1) 337 8055 - Fax (+57 1) 2686379</span>
                <span>Email: kelinda@kelinda.com - Todos los derechos reservados</span>
            </div>
        </div>
    </footer>
{/if}