<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class MyCategories extends Module
{
	public function __construct()
	{
            $this->name = 'mycategories';
            $this->tab = 'front_office_features';
            $this->version = '2.8.3';
            $this->author = 'Fabian Morales';

            $this->bootstrap = true;
            parent::__construct();

            $this->displayName = 'My categorias';
            $this->description = 'Bloque de categorias tipo galeria';
            $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
            if (!parent::install() ||
                !$this->registerHook('displayHomeTabContent') ||
                !$this->registerHook('displayHomeTab') ||
                !$this->registerHook('header'))
                // Temporary hooks. Do NOT hook any module on it. Some CRUD hook will replace them as soon as possible.
                return false;

            return true;
	}

	public function uninstall()
	{
	}

	public function hookDisplayHomeTabContent($params)
	{
            return $this->hookDisplayHomeTab($params);
	}
    
	public function hookDisplayHomeTab($params)
	{        
            $categorias = Category::getCategories((int)($params["id_lang"]), true, false, ' and c.level_depth > 1', ' order by c.id_category ', ' limit 0, 8 ');
            $this->smarty->assign(array('categorias' => $categorias));
            $display = $this->display(__FILE__, 'mycategories.tpl');
            return $display;
	}

	public function hookDisplayHeader()
	{
            $this->context->controller->addCSS(($this->_path).'mycategories.css', 'all');
	}
}
