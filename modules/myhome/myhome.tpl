{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo home -->
<div class="container_home">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="contenido home">
                    <img src="{$base_dir}themes/kelinda/imagenes/logo_blanco.png" class="text-center" />
                    <a class="menu-resp home" href="#" data-menu="menu_home">Menu</a>
                    <ul class="menu home" id="menu_home">
                        <li><a href="{$base_dir}index.php?controller=custom&tarea=subhome">Inicio</a></li>
                        <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=3">Mujeres</a></li>
                        <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=45">Hombres</a></li>
                        <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=47">Ni&ntilde;as</a></li>
                        <li><a href="{$base_dir}index.php?controller=custom&tarea=cathome&id_cat=46">Ni&ntilde;os</a></li>
                        <li><a href="{$base_dir}index.php?controller=custom&tarea=catalogo">Catalogo</a></li>
                        <li><a href="{$base_dir}index.php?controller=custom&tarea=tiendas">Tiendas</a></li>
                        <li><a href="{$base_dir}index.php?controller=custom&tarea=mostrarContacto">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="categorias home hidden-sm">
        <div class="container">
            <!--ul class="row">
                <li class="col-md-2"><a href="https://www.instagram.com/kelindaswimwear/" target="_blank"><img src="{$base_dir}themes/kelinda/imagenes/categoria_thumb1.jpg" /></a></li>
                <li class="col-md-2"><a href="{$base_dir}3-mujer"><img src="{$base_dir}themes/kelinda/imagenes/categoria_thumb2.jpg" /></a></li>
                <li class="col-md-2"><a href="{$base_dir}3-mujer"><img src="{$base_dir}themes/kelinda/imagenes/categoria_thumb3.jpg" /></a></li>
                <li class="col-md-2"><a href="{$base_dir}3-mujer"><img src="{$base_dir}themes/kelinda/imagenes/categoria_thumb4.jpg" /></a></li>
                <li class="col-md-2"><a href="{$base_dir}3-mujer"><img src="{$base_dir}themes/kelinda/imagenes/categoria_thumb5.jpg" /></a></li>
                <li class="col-md-2"><a href="https://www.instagram.com/kelindaswimwear/" target="_blank"><img src="{$base_dir}themes/kelinda/imagenes/categoria_thumb1.jpg" /></a></li>
            </ul-->
            {hook h='displayBannerHome'}
        </div>
    </div>
</div>
